/*
 * speed.h
 *
 * Created: 5/1/2016 7:53:28 AM
 *  Author: xbarin08
 */ 


#ifndef SPEED_H_
#define SPEED_H_

#include "globals.h"

uint16_t speed;

void speed_init();

inline void speed_measure()
{
	speed=TCC1.CNT;
	TCC1.CNT=0;
	tx_buf[2]=(speed & 0xff00) >> 8;
	tx_buf[3]=(speed & 0x00ff);
}

inline uint16_t get_speed()
{
	return speed;
}

#endif /* SPEED_H_ */