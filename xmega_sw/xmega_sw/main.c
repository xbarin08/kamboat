/*
* xmega_sw.c
*
* Created: 4/16/2016 9:34:05 AM
* Author : xbarin08
*/

#include "globals.h"
#include "komutace.h"
#include "startup.h"
#include "speed.h"

void freq_init()
{
	OSC_XOSCCTRL = OSC_FRQRANGE_12TO16_gc | OSC_XOSCSEL_XTAL_256CLK_gc;
	OSC_CTRL |= OSC_XOSCEN_bm;
	while(!(OSC.STATUS & OSC_XOSCRDY_bm));
	OSC_PLLCTRL = OSC_PLLSRC_XOSC_gc | 2;
	OSC_CTRL |= OSC_PLLEN_bm;
	while(!(OSC.STATUS & OSC_PLLRDY_bm));
	CCP = CCP_IOREG_gc;
	CLK_CTRL = CLK_SCLKSEL_PLL_gc;
}

int16_t filtr_pwm(int16_t pwm_in)
{
	if(abs(pwm_in) < abs(pwm_prev))
	{
		return pwm_in;
	}
	if(pwm_in==0)
	{
		return 0;
	}
	float a=0.320300733393298;
	if(im>10000)
	{
		pwm_red=pwm_red+3;
	}
	else
	{
		pwm_red=pwm_red-3;
	}
	if(pwm_red<0)
	{
		pwm_red=0;
	}
	if(pwm_red>256)
	{
		pwm_red=256;
	}
	int16_t pwm_tmp=a*pwm_in+(1-a)*pwm_prev;
	int16_t pwm_out=0;
	if(pwm_tmp<0)
	{
		pwm_out=pwm_tmp+pwm_red;
		if(pwm_out>0)
		{
			pwm_out=0;
		}
	}
	if(pwm_tmp>0)
	{
		pwm_out=pwm_tmp-pwm_red;
		if(pwm_out<0)
		{
			pwm_out=0;
		}
	}
	return pwm_out;	
}

int16_t filtr_bemf(int16_t bemf_in)
{
	int16_t bemf_out;
	float a=0.616637244141723;
	bemf_out=a*bemf_in+(1-a)*bemf_prev;
	return bemf_out;
}

ISR(ADCA_CH0_vect)
{
	int16_t tmp;
	tmp=(int16_t)ADCA.CH0.RES-vcc/2;
	if((hrana*smer*tmp)>0)
	{
		pruchod=true;
	}
	if(pruchod)
	{
		bemf_int=(tmp/2)+bemf_int;//filtr_bemf(tmp/2)+bemf_int;
		if(abs(bemf_int)>=0x2354)
		{
			bemf_int=0;
			komutuj();
			speed_measure();
			startup_set_per(get_speed());
			pruchod=false;
			//PORTE.OUTTGL=PIN0_bm;
		}
	}
}

ISR(ADCA_CH1_vect)
{
	vcc=(int16_t)ADCA.CH1.RES;
	tx_buf[5]=(vcc & 0xff00) >> 8;
	tx_buf[6]=(vcc & 0x00ff);
}

ISR(ADCA_CH2_vect)
{
	int16_t im_in=(int16_t)ADCA.CH2.RES;
	double a=0.2868;
	im=(int16_t)(a*im_in+(1-a)*im_in);
	tx_buf[1]=(im & 0xff00) >> 8;
	tx_buf[2]=(im & 0x00ff);
	im_prev=im;
}

ISR(ADCA_CH3_vect)
{
	temp=(int16_t)ADCA.CH3.RES;
}

ISR(USARTD0_RXC_vect)
{
	
}

ISR(USARTD0_TXC_vect)
{
	
}

ISR(USARTD0_DRE_vect)
{
	
}

ISR(TCD0_OVF_vect)
{
	DMA.CH1.CTRLA |= DMA_CH_ENABLE_bm;
}

ISR(DMA_CH0_vect)
{
	if(DMA.CH0.CTRLB & DMA_CH_TRNIF_bm)
	{
		if((uart_buf[0]+uart_buf[1]+uart_buf[2]+uart_buf[3]+uart_buf[4]+uart_buf[5])!=((uart_buf[6] << 8 )|uart_buf[7]))
		{
			return;
		}
		uint16_t uart = uart_buf[0] << 8 | uart_buf[1];
		pwm_prev = filtr_pwm(uart);
		uint16_t pwm = pwm_prev+256;
		if(pwm>256)
		{
			smer=1;
			asmsk();
			bsmsk();
			csmsk();
		}
		else if(pwm<256)
		{
			smer=-1;
			asmsk();
			bsmsk();
			csmsk();
		}
		else
		{
			smer=0;
			startup_reset();
			aumsk();
			bumsk();
			cumsk();
		}
		cli();
		if(AMASK&AOUT)
		{
			SSDA;
		}
		else
		{
			CSDA;
		}
		if(BMASK&BOUT)
		{
			SSDB;
		}
		else
		{
			CSDB;
		}
		if(CMASK&COUT)
		{
			SSDC;
		}
		else
		{
			CSDC;
		}
		sei();
		cli();
		TCC0.CCABUF = pwm;
		TCC0.CCCBUF = pwm;
		TCC0.CCDBUF = pwm;
		sei();
		uint16_t servo = (uint16_t)uart_buf[2] << 8 | uart_buf[3];
		TCD1.CCABUF = servo;
		DMA.CH0.CTRLB |= DMA_CH_TRNIF_bm;
	}
}

int main(void)
{
	globals_init();
	freq_init();

	PORTD.DIR=PIN4_bm | PIN3_bm;
	PORTC.DIR=PIN0_bm | PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm;
	PORTB.DIR=PIN2_bm;
	PORTE.DIR=PIN0_bm | PIN1_bm;
	
	komutace_init();

	TCC0.CTRLA = TC_CLKSEL_DIV1_gc;
	TCC0.CTRLB = TC0_CCAEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC_WGMODE_DS_B_gc;
	TCC0.CTRLD = TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;
	TCC0.CTRLE = 0;
	TCC0.PER = 0x200;
	TCC0.CCA = 0x100;
	TCC0.CCC = 0x100;
	TCC0.CCD = 0x100;

	TCD1.CTRLA = TC_CLKSEL_DIV4_gc;
	TCD1.CTRLB = TC1_CCAEN_bm | TC_WGMODE_SS_gc;
	TCD1.CTRLD = TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;
	TCD1.CTRLE = 0;
	TCD1.PER = 0xffff;
	TCD1.CCA = 0x3000;
	
	TCD0.CTRLA = TC_CLKSEL_DIV8_gc;
	TCD0.CTRLB = TC_WGMODE_NORMAL_gc;
	TCD0.CTRLD = TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;
	TCD0.INTCTRLA = TC_OVFINTLVL_LO_gc;
	TCD0.PER = 0xffff;
	
	startup_init();
	
	speed_init();

	ADCA.CTRLA = ADC_ENABLE_bm;
	ADCA.CTRLB = ADC_FREERUN_bm | ADC_RESOLUTION_LEFT12BIT_gc | ADC_CONMODE_bm;
	ADCA.REFCTRL = ADC_REFSEL_AREFB_gc;
	ADCA.EVCTRL = ADC_SWEEP_0123_gc | ADC_EVACT_NONE_gc;
	ADCA.PRESCALER = ADC_PRESCALER_DIV512_gc;

	ADCA.CH0.CTRL = ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN0_gc;
	ADCA.CH0.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_HI_gc;

	ADCA.CH1.CTRL = ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH1.MUXCTRL = ADC_CH_MUXPOS_PIN5_gc;
	ADCA.CH1.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_HI_gc;

	ADCA.CH2.CTRL = ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH2.MUXCTRL = ADC_CH_MUXPOS_PIN3_gc;
	ADCA.CH2.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_HI_gc;

	ADCA.CH3.CTRL = ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH3.MUXCTRL = ADC_CH_MUXPOS_PIN4_gc;
	ADCA.CH3.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_HI_gc;


	ADCA.CTRLA |= ADC_CH0START_bm | ADC_CH1START_bm | ADC_CH2START_bm | ADC_CH3START_bm;

	PMIC.CTRL = PMIC_HILVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_LOLVLEN_bm;
	
	//USARTD0.CTRLA =  USART_DREINTLVL_LO_gc;
	USARTD0.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | USART_CHSIZE_8BIT_gc;
	USARTD0.BAUDCTRLA = 75;
	USARTD0.BAUDCTRLB = ((-6) << USART_BSCALE_gp);
	USARTD0.CTRLB = USART_RXEN_bm | USART_TXEN_bm;
	
	DMA.CH0.REPCNT = 0;
	DMA.CH0.CTRLA =  DMA_CH_SINGLE_bm | DMA_CH_REPEAT_bm | DMA_CH_BURSTLEN_1BYTE_gc;
	DMA.CH0.CTRLB = DMA_CH_TRNINTLVL_LO_gc;
	DMA.CH0.TRIGSRC = DMA_CH_TRIGSRC_USARTD0_RXC_gc;
	DMA.CH0.ADDRCTRL = DMA_CH_SRCDIR_FIXED_gc | DMA_CH_DESTRELOAD_BLOCK_gc| DMA_CH_DESTDIR_INC_gc;
	DMA.CH0.SRCADDR0 = ((uint16_t)&USARTD0.DATA) & 0xff;
	DMA.CH0.SRCADDR1 = (uint16_t)&USARTD0.DATA >> 8;
	DMA.CH0.SRCADDR0 = (((uint32_t)&USARTD0.DATA) >> 16);
	DMA.CH0.DESTADDR0 = (((uint16_t) & uart_buf));
	DMA.CH0.DESTADDR1 = (((uint16_t) & uart_buf >> 8));
	DMA.CH0.DESTADDR2 = (((uint32_t) & uart_buf) >> 16);
	DMA.CH0.TRFCNT = 8;
	DMA.CH0.CTRLA |= DMA_CH_ENABLE_bm;
	
	DMA.CH1.REPCNT = 0;
	DMA.CH1.CTRLA =  DMA_CH_SINGLE_bm /*| DMA_CH_REPEAT_bm*/ | DMA_CH_BURSTLEN_1BYTE_gc;
	//DMA.CH1.CTRLB = DMA_CH_TRNINTLVL_LO_gc;
	DMA.CH1.TRIGSRC = DMA_CH_TRIGSRC_USARTD0_DRE_gc;
	DMA.CH1.ADDRCTRL = DMA_CH_SRCDIR_INC_gc | DMA_CH_SRCRELOAD_BLOCK_gc | DMA_CH_DESTDIR_FIXED_gc;
	DMA.CH1.DESTADDR0 = ((uint16_t)&USARTD0.DATA) & 0xff;
	DMA.CH1.DESTADDR1= (uint16_t)&USARTD0.DATA >> 8;
	DMA.CH1.DESTADDR2= (((uint32_t)&USARTD0.DATA) >> 16);
	DMA.CH1.SRCADDR0 = (((uint16_t) & tx_buf));
	DMA.CH1.SRCADDR1 = (((uint16_t) & tx_buf >> 8));
	DMA.CH1.SRCADDR2 = (((uint32_t) & tx_buf) >> 16);
	DMA.CH1.TRFCNT = 8;
	DMA.CH1.CTRLA |= DMA_CH_ENABLE_bm;
	
	DMA.CTRL = DMA_ENABLE_bm;
	
	WDT.CTRL = WDT_PER_2KCLK_gc;
	WDT.CTRL = WDT_CEN_bm | WDT_ENABLE_bm;

	sei();
	
	komutuj();
	
	
	
	/* Replace with your application code */
	while (1)
	{
		wdt_reset();
	}
}

