/*
 * globals.h
 *
 * Created: 5/1/2016 7:36:06 AM
 *  Author: xbarin08
 */ 


#ifndef GLOBALS_H_
#define GLOBALS_H_

#define F_CPU 32000000L

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <stdint-gcc.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

volatile int8_t natoceni;
volatile int8_t hrana,smer;
volatile int16_t vcc;
volatile int16_t im;
volatile int16_t temp;
uint8_t pruchod;
int16_t bemf_int;
char uart_buf[8];
int16_t pwm_prev;
int16_t bemf_prev;
int16_t im_prev;
int16_t pwm_red;
volatile uint8_t tx_buf[8];

void globals_init();

#endif /* GLOBALS_H_ */