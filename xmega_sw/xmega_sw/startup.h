/*
 * startup.h
 *
 * Created: 5/1/2016 7:29:56 AM
 *  Author: xbarin08
 */ 

#ifndef STARTUP_H_
#define STARTUP_H_

#include "globals.h"
#include "komutace.h"

void startup_init();

inline void startup_clear()
{
	TCE0.CNT=0;
}

inline void startup_reset()
{
	TCE0.CNT=0;
	TCE0.PER=0xffff;
}

inline void startup_set_per(uint16_t per)
{
	if(per>(0xffff-1))
	{
		TCE0.PERBUF=0xffff;
		return;
	}
	TCE0.PERBUF=per+1;
}

#endif /* STARTUP_H_ */