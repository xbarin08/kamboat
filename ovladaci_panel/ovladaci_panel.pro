#-------------------------------------------------
#
# Project created by QtCreator 2016-04-22T20:06:43
#
#-------------------------------------------------

QT       += core gui network gamecontroller

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = ovladaci_panel
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
