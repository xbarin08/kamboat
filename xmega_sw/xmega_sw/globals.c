/*
 * globals.c
 *
 * Created: 5/1/2016 7:16:49 PM
 *  Author: xbarin08
 */ 

#include "globals.h"

void globals_init()
{
	natoceni=0;
	hrana=0;
	smer=0;
	vcc=31279;
	im=31279;
	temp=31279;
	pruchod=false;
	bemf_int=0;
	pwm_prev=0;
	bemf_prev=0;
	im_prev=0;
	tx_buf[0]=85;
	tx_buf[7]=42;
	pwm_red=0;
}
