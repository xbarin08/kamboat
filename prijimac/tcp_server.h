#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QtSerialPort/QtSerialPort>
#include <cstring>
#include <QTimer>
#include <iostream>
#include <QString>
#include <iomanip>
#include <cinttypes>
#include <cstdint>

class tcp_server : public QObject
{
    Q_OBJECT
public:
    explicit tcp_server(QObject *parent = 0);

    virtual ~tcp_server();

signals:

public slots:
    void prichozi_spojeni();

    void prichozi_data();

    void on_uart_ready_read();

    void on_disconnected();

    void on_error(QAbstractSocket::SocketError socketError);

private:

    QTcpServer server;

    QTcpSocket *socket;

    QByteArray tcp_buffer;

    QByteArray uart_buffer;

    unsigned char serial_data[8];

    QSerialPort uart;

    bool servo_rdy,pwm_rdy;
};

#endif // TCP_SERVER_H
