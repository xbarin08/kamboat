#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <cstring>
#include <QMainWindow>
#include <QTcpSocket>
#include <QHostAddress>
#include <QStringList>
#include <QMessageBox>
#include <QByteArray>
#include <QList>
#include <QTimer>

#include <QtGameController/QGameController>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_servo_valueChanged(int value);

    void on_pwm_valueChanged(int value);

    void on_pushButton_clicked();

    void on_comboBox_currentIndexChanged(int index);

    void handleQGameControllerAxisEvent(QGameControllerAxisEvent* event);

    void handleQGameControllerButtonEvent(QGameControllerButtonEvent* event);

    void on_socket_timeout();

    void on_socket_read();

signals:

    void joystick_read();

private:
    Ui::MainWindow *ui;

    QTcpSocket socket;

    QList<QGameController*> controllers;

    int prev_index;

    QTimer *joy_timer, *socket_timer;

    unsigned char pwm[4],servo[4];

    QByteArray rx_buffer;

    QByteArray b_beg;

    double ot_prev,i_prev,vcc_prev;

};

#endif // MAINWINDOW_H
