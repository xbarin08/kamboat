/*
 * startup.c
 *
 * Created: 5/1/2016 7:30:35 AM
 *  Author: xbarin08
 */ 

#include "startup.h"

ISR(TCE0_OVF_vect)
{
	if(smer==0)
	{
		return;
	}
	if(TCE0.PER>10)
	{
		TCE0.PERBUF=TCE0.PERBUF-10;
	}
	komutuj();
}

void startup_init()
{
	TCE0.CTRLA = TC_CLKSEL_DIV64_gc;
	TCE0.CTRLB = TC_WGMODE_NORMAL_gc;
	TCE0.CTRLD = TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;
	TCE0.INTCTRLA = TC_OVFINTLVL_LO_gc;
	TCE0.PER = 0x2000;
}
