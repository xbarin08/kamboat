/*
 * speed.c
 *
 * Created: 5/1/2016 7:53:39 AM
 *  Author: xbarin08
 */ 

#include "speed.h"

ISR(TCC1_OVF_vect)
{
	speed=0xffff;
	tx_buf[2]=(speed & 0xff00) >> 8;
	tx_buf[3]=(speed & 0x00ff);
}

void speed_init()
{
	TCC1.CTRLA = TC_CLKSEL_DIV64_gc;
	TCC1.CTRLB = TC_WGMODE_NORMAL_gc;
	TCC1.CTRLD = TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;
	TCC1.INTCTRLA = TC_OVFINTLVL_LO_gc;
	TCC1.PER = 0xffff;
	speed=0xffff;
}
