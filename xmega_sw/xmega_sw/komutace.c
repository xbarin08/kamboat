/*
 * komutace.c
 *
 * Created: 5/1/2016 7:45:04 AM
 *  Author: xbarin08
 */ 

#include "komutace.h"

void komutuj(void)
{
	switch(natoceni)
	{
		case 0:
		adis();
		bdis();
		cdis();
		APOS;
		BNEG;
		aena();
		bena();
		hrana=-1;
		ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN2_gc;
		break;
		case 1:
		adis();
		bdis();
		cdis();
		APOS;
		CNEG;
		aena();
		cena();
		hrana=1;
		ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN1_gc;
		break;
		case 2:
		adis();
		bdis();
		cdis();
		BPOS;
		CNEG;
		bena();
		cena();
		hrana=-1;
		ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN0_gc;
		break;
		case 3:
		adis();
		bdis();
		cdis();
		BPOS;
		ANEG;
		bena();
		aena();
		hrana=1;
		ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN2_gc;
		break;
		case 4:
		adis();
		bdis();
		cdis();
		CPOS;
		ANEG;
		cena();
		aena();
		hrana=-1;
		ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN1_gc;
		break;
		case 5:
		adis();
		bdis();
		cdis();
		CPOS;
		BNEG;
		cena();
		bena();
		hrana=1;
		ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN0_gc;
		break;
		default:
		adis();
		bdis();
		cdis();
		break;
	}
	if(smer<0)
	{
		--natoceni;
	}
	else if(smer>0)
	{
		++natoceni;
	}
	if(natoceni<0)
	{
		natoceni=5;
	}
	else if(natoceni>5)
	{
		natoceni=0;
	}
	startup_clear();
}