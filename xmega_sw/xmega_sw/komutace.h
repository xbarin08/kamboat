/*
 * shutdown.h
 *
 * Created: 4/29/2016 6:25:40 PM
 *  Author: xbarin08
 */ 


#ifndef KOMUTACE_H_
#define KOMUTACE_H_

#include "globals.h"
#include "startup.h"
#include "speed.h"

#define SSDA (PORTB.OUTCLR = PIN2_bm)
#define CSDA (PORTB.OUTSET = PIN2_bm)
#define SSDB (PORTC.OUTCLR = PIN1_bm)
#define CSDB (PORTC.OUTSET = PIN1_bm)
#define SSDC (PORTC.OUTCLR = PIN4_bm)
#define CSDC (PORTC.OUTSET = PIN4_bm)

#define AENA (masky |= 1)
#define ADIS (masky &= ~1)
#define BENA (masky |= 2)
#define BDIS (masky &= ~2)
#define CENA (masky |= 4)
#define CDIS (masky &= ~4)

#define AOUT (1&masky)
#define BOUT (2&masky)
#define COUT (4&masky)

#define ASMSK (masky |= (1 * 0x10))
#define AUMSK (masky &= ~(1 * 0x10))
#define BSMSK (masky |= (2 * 0x10))
#define BUMSK (masky &= ~(2 * 0x10))
#define CSMSK (masky |= (4 * 0x10))
#define CUMSK (masky &= ~(4 * 0x10))

#define AMASK (1&(masky / 0x10))
#define BMASK (2&(masky / 0x10))
#define CMASK (4&(masky / 0x10))

#define APOS (PORTC.PIN0CTRL |= PORT_INVEN_bm)
#define ANEG (PORTC.PIN0CTRL &= ~PORT_INVEN_bm)
#define BPOS (PORTC.PIN2CTRL |= PORT_INVEN_bm)
#define BNEG (PORTC.PIN2CTRL &= ~PORT_INVEN_bm)
#define CPOS (PORTC.PIN3CTRL |= PORT_INVEN_bm)
#define CNEG (PORTC.PIN3CTRL &= ~PORT_INVEN_bm)

volatile uint8_t masky;

inline void aena()
{
	AENA;
	if(AMASK)
	{
		SSDA;
	}
}

inline void adis()
{
	ADIS;
	CSDA;
}

inline void bena()
{
	BENA;
	if(BMASK)
	{
		SSDB;
	}
}

inline void bdis()
{
	BDIS;
	CSDB;
}

inline void cena()
{
	CENA;
	if(CMASK)
	{
		SSDC;
	}
}

inline void cdis()
{
	CDIS;
	CSDC;
}

inline void asmsk()
{
	ASMSK;
	if(AOUT)
	{
		SSDA;
	}
}

inline void aumsk()
{
	AUMSK;
	CSDA;
}


inline void bsmsk()
{
	BSMSK;
	if(BOUT)
	{
		SSDB;
	}
}

inline void bumsk()
{
	BUMSK;
	CSDB;
}

inline void csmsk()
{
	CSMSK;
	if(COUT)
	{
		SSDC;
	}
}

inline void cumsk()
{
	CUMSK;
	CSDC;
}

inline void komutace_init()
{
	adis();
	bdis();
	cdis();
	aumsk();
	bumsk();
	cumsk();
}

void komutuj(void);

#endif /* KOMUTACE_H_ */