#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QGameController *controller;
    controllers.clear();
    for(int i=0;i<10;++i)
    {
        controller = new QGameController(i,this);
        if(controller->isValid())
        {
            controllers.append(controller);
        }
    }
    joy_timer = new QTimer(this);
    joy_timer->setInterval(15);
    socket_timer = new QTimer(this);
    socket_timer->setInterval(15);
    connect(socket_timer,&QTimer::timeout,this,&MainWindow::on_socket_timeout);
    prev_index=0;
    connect(controllers[0],&QGameController::gameControllerAxisEvent,this,&MainWindow::handleQGameControllerAxisEvent);
    connect(controllers[0],&QGameController::gameControllerButtonEvent,this,&MainWindow::handleQGameControllerButtonEvent);
    connect(joy_timer,&QTimer::timeout,controllers[0],&QGameController::readGameController);
    for(int i=0;i<controllers.size();++i)
    {
        ui->comboBox->addItem(controllers.at(i)->description());
    }
    joy_timer->start();
    b_beg.clear();
    b_beg.append(85);
    b_beg.append(85);
    on_servo_valueChanged(0);
    on_pwm_valueChanged(0);
    ot_prev=i_prev=vcc_prev=0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_servo_valueChanged(int value)
{
    uint16_t tmp=value/250+12000;
    servo[0]=160;
    servo[1]=(tmp & 0xff00) >> 8;
    servo[2]=(tmp & 0x00ff);
    servo[3]=85;
}

void MainWindow::on_pwm_valueChanged(int value)
{
    int16_t tmp=value/3906.25;
    pwm[0]=161;
    pwm[1]=(tmp & 0xff00) >> 8;
    pwm[2]=(tmp & 0x00ff);
    pwm[3]=85;
}

void MainWindow::on_pushButton_clicked()
{
    QStringList tmp = ui->lineEdit->text().split(":",QString::KeepEmptyParts);
    socket.connectToHost(QHostAddress(tmp[0]),tmp[1].toInt(),QIODevice::ReadWrite);
    if(!socket.waitForConnected())
    {
        QMessageBox::critical(this,"Error",socket.errorString());
    }
    socket_timer->start();
    connect(&socket,&QTcpSocket::readyRead,this,&MainWindow::on_socket_read);
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    if(prev_index == index)
    {
        return;
    }
    joy_timer->stop();
    disconnect(joy_timer,&QTimer::timeout,controllers[prev_index],&QGameController::readGameController);
    disconnect(controllers[prev_index],&QGameController::gameControllerAxisEvent,this,&MainWindow::handleQGameControllerAxisEvent);
    disconnect(controllers[prev_index],&QGameController::gameControllerButtonEvent,this,&MainWindow::handleQGameControllerButtonEvent);
    connect(controllers[index],&QGameController::gameControllerAxisEvent,this,&MainWindow::handleQGameControllerAxisEvent);
    connect(controllers[index],&QGameController::gameControllerButtonEvent,this,&MainWindow::handleQGameControllerButtonEvent);
    connect(joy_timer,&QTimer::timeout,controllers[index],&QGameController::readGameController);
    prev_index=index;
    joy_timer->start();
}

void MainWindow::handleQGameControllerAxisEvent(QGameControllerAxisEvent *event)
{
    //qDebug() << event->value();
    if(event->axis()==2)
    {
        ui->servo->setValue(event->value()*1000000);
    }
    if(event->axis()==1)
    {
        ui->pwm->setValue(event->value()*-1000000);
    }
    delete event;
}

void MainWindow::handleQGameControllerButtonEvent(QGameControllerButtonEvent *event)
{

    delete event;
}

void MainWindow::on_socket_timeout()
{
    char tmp[4];
    socket.waitForBytesWritten();
    std::memcpy(tmp,servo,4);
    socket.write(tmp,4);
    std::memcpy(tmp,pwm,4);
    socket.write(tmp,4);
}

void MainWindow::on_socket_read()
{
    rx_buffer.append(socket.readAll());
    int begin = rx_buffer.lastIndexOf(85);
    if(begin<0)
    {
        return;
    }
    rx_buffer.remove(0,begin);
    if((rx_buffer.size()>=8) && (rx_buffer.at(7)==42) && (rx_buffer.at(0)==85))
    {
        int tmp = int16_t((rx_buffer.at(1) << 8)) + uint8_t(rx_buffer.at(2));
        double i = 33000*(tmp/32768.0)+66.2557;
        int tmp2 = uint16_t((rx_buffer.at(3) << 8)) + uint8_t(rx_buffer.at(4));
        double ot = 0;
        if(tmp2!=0)
        {
            ot = 60/(6.0*(tmp2)*2e-6);
        }
        int tmp3 = int16_t((rx_buffer.at(5) << 8)) + uint8_t(rx_buffer.at(6));
        double vcc = (4000*3.3*(tmp3/32768.0))*1.1578-3904.7;


        double a=0.1407753643;
        ot=a*ot+(1-a)*ot_prev;
        i=a*i+(1-a)*i_prev;
        vcc=a*vcc+(1-a)*vcc_prev;

        ui->bar_otacky->setValue(ot);
        ui->bar_proud->setValue(i);
        ui->bar_napeti->setValue(vcc);
        qDebug() << tmp2;
        qDebug() << ot;
        rx_buffer.clear();

        ot_prev=ot;
        i_prev=i;
        vcc_prev=vcc;
    }
    else
    {
        rx_buffer.clear();
    }
}
