QT += core network serialport
QT -= gui

CONFIG += c++11

TARGET = prijimac
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    tcp_server.cpp

HEADERS += \
    tcp_server.h
