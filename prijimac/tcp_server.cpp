#include "tcp_server.h"

tcp_server::tcp_server(QObject *parent) : QObject(parent)
{
    connect(&server,&QTcpServer::newConnection,this,&tcp_server::prichozi_spojeni);

    if(!server.listen(QHostAddress::Any, 2000))
    {
        qDebug() << "Server could not start";
    }
    else
    {
        qDebug() << "Server started!";
    }
    std::memset(serial_data,0,sizeof(serial_data));
    uart.setPortName("/dev/ttyS0");
    uart.setBaudRate(921600);
    uart.setParity(QSerialPort::NoParity);
    if(!uart.open(QIODevice::ReadWrite))
    {
        qDebug() << "uart" << uart.errorString();
    }
    servo_rdy=pwm_rdy=false;
    tcp_buffer.clear();
    connect(&uart,&QSerialPort::readyRead,this,&tcp_server::on_uart_ready_read);
    socket=nullptr;
}

tcp_server::~tcp_server()
{
    uart.close();
    socket->close();
    server.close();
}

void tcp_server::prichozi_spojeni()
{
    socket = server.nextPendingConnection();
    connect(socket,&QTcpSocket::readyRead,this,&tcp_server::prichozi_data);
    connect(socket,&QTcpSocket::disconnected,this,&tcp_server::on_disconnected);
    connect(socket,static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),this,&tcp_server::on_error);
}

void tcp_server::prichozi_data()
{
    tcp_buffer.append(socket->readAll());
    tcp_buffer.remove(tcp_buffer.size()-8,tcp_buffer.size()-8);
    uart_buffer=tcp_buffer;
    unsigned char data[8];
    std::memcpy(data,uart_buffer.data()+uart_buffer.size()-8,8);
    uart_buffer.clear();
    tcp_buffer.clear();
    if(data[0]==160)
    {
        serial_data[2]=data[1];
        serial_data[3]=data[2];
        servo_rdy=true;
    }
    if(data[0]==161)
    {
        serial_data[0]=data[1];
        serial_data[1]=data[2];
        pwm_rdy=true;
    }
    if(data[4]==160)
    {
        serial_data[2]=data[5];
        serial_data[3]=data[6];
        servo_rdy=true;
    }
    if(data[4]==161)
    {
        serial_data[0]=data[5];
        serial_data[1]=data[6];
        pwm_rdy=true;
    }
    char tmp[8];
    if(!servo_rdy || !pwm_rdy)
    {
        return;
    }
    uart.waitForBytesWritten(30000);
    serial_data[6]=((serial_data[0]+serial_data[1]+serial_data[2]+serial_data[3]+serial_data[4]+serial_data[5]) & 0xff00) >> 8;
    serial_data[7]=((serial_data[0]+serial_data[1]+serial_data[2]+serial_data[3]+serial_data[4]+serial_data[5]) & 0x00ff);
    std::memcpy(tmp,serial_data,8);
    uart.write(tmp,8);
}

void tcp_server::on_uart_ready_read()
{
    if(socket==nullptr)
    {
        uart.clear();
        return;
    }
    if(!socket->isOpen())
    {
        uart.clear();
        return;
    }
    socket->waitForBytesWritten();
    socket->write(uart.readAll());
}

void tcp_server::on_disconnected()
{
    disconnect(socket,&QTcpSocket::readyRead,this,&tcp_server::prichozi_data);
    disconnect(socket,&QTcpSocket::disconnected,this,&tcp_server::on_disconnected);
    socket=nullptr;
    char tmp[8];
    std::memset(tmp,0,8);
    uart.write(tmp,8);
}

void tcp_server::on_error(QAbstractSocket::SocketError socketError)
{
    char tmp[8];
    std::memset(tmp,0,8);
    uart.write(tmp,8);
}
